
#include <stdio.h>

#include <b/b.h>
#include <b/b_priv.h>

int b_value = 101;

void decprint_b(void)
{
    printb();
    decb();
}

void printb(void)
{
    printf("For B :: %d\n", b);
}
