

all: test

CC=~/install/bin/clang

test: liba.a libb.a main.o
	$(CC) -fmodules -fmodule-map-file=module.map -fmodule-maps main.o -L. -o $@

liba.a: a/a.o
libb.a: b/b.o

%.o:%.c
	$(CC) -fmodules -fmodule-maps -x objective-c -fmodule-map-file=module.map -I. -c $^ -o $@ 

lib%.a:
	ar rcs $@ $^

clean:
	rm -f *.o *.a a/*.o b/*.o
	rm -f test
